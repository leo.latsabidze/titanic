import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    final = []
    x =  ["Mr.", "Mrs.", "Miss."]
    for y in x:
        data = df[df['Name'].str.contains(y, na=False)]
        med_age = data['Age'].median()
        blanks = data['Age'].isna().sum()
        final.append((y, blanks, round(med_age)))
    return None
